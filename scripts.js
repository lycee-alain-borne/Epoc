function Verso(element,numero){
    document.getElementById(numero).getElementsByClassName('resume')[0].style.display="block";
    document.getElementById(numero).getElementsByClassName('qrcode')[0].style.display="none";
    document.getElementById(numero).style.transform='rotateY(180deg)';
}

function Recto(element,numero){
    document.getElementById(numero).style.transform='rotateY(0deg)';
}


function search_epoc() {
    let input = document.getElementById('searchbar').value
    input=input.toLowerCase();
    let x = document.getElementsByClassName('filtre');
    console.log(x);
      
    for (i = 0; i < x.length; i++) { 
        console.log(input);
        if (!x[i].innerHTML.toLowerCase().includes(input)) {
            x[i].parentElement.parentElement.style.display="none";
        }
        else {
            x[i].parentElement.parentElement.style.display="block";                 
        }
    }
}

function myQR(element,numero){
    document.getElementById(numero).getElementsByClassName('resume')[0].style.display="none";
    document.getElementById(numero).getElementsByClassName('qrcode')[0].style.display="block";
    document.getElementById(numero).style.transform='rotateY(180deg)';
}